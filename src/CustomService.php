<?php
namespace Baiyuwan\Wenkai;

trait CustomService
{
    public static function module(){
        $arr = explode('\\',  get_called_class());
        return strtolower($arr[3]) .'.' .strtolower(str_replace('Controller','',end($arr))) . ".";
    }

}
