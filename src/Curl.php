<?php
namespace Baiyuwan\Wenkai;

class Curl
{
    public static $url;    //访问的URL
    public static $ip,$port,$proxy,$header;

    //错误信息
    private static $curlError;
    //header头信息
    private static $headerStr;
    //请求状态
    private static $status;

    use JsonService;
    /*
     * $args 数组 或者 需要访问的URL地址
     * */
    public function __construct($args=null,$ip=null,$port=null)
    {
        if($args != null){
            if(is_array($args)){
                $this->setUrl($args['url']);
                $this->setIp($args['ip']);
                $this->setPort($args['port']);
            }else{
                $this->setUrl($args);
                $ip && $this->setIp($ip);
                $port && $this->setIp($port);
            }
        }

        if($this->getIp() && $this->getPort()){
            $this->proxy = "http://" . $this->getIp() . ":" . $this->getPort();
        }
    }

    public static function downFile($url='', $method = 'get',  $timeout = 100){
        $url && self::setUrl($url);
        if(empty(self::getUrl())){
            self::$curlError = "请求地址不能为空！";
            return ;
        }
        $curl = curl_init();
        if(self::getProxy()){
            curl_setopt ($curl, CURLOPT_PROXY, self::getProxy());//设置通过的HTTP代理服务器

        }
        if(self::getIp()){
            //curl_setopt ($curl, CURLOPT_HTTPHEADER, ['X-FORWARDED-FOR:'.self::getIp(),'CLIENT-IP:'.self::getIp()]);//此处可以改为任意假IP
            self::$header = array_merge(['X-FORWARDED-FOR:'.self::getIp(),'CLIENT-IP:'.self::getIp()],self::$header);
        }
        $method = strtoupper($method);
        //请求方式
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        //post请求
        if ($method == 'POST') curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        //超时时间 //CURLOPT_TIMEOUT 可以设置为100秒，如果文件100秒内没有下载完成，脚本将会断开连接    ,如果想让人卡死  设置的大一点
        curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
        //设置header头
        if ($header !== false) curl_setopt($curl, CURLOPT_HTTPHEADER, $header);

        curl_setopt ($curl, CURLOPT_USERAGENT, self::getAgent());//在HTTP请求中包含一个”user-agent”头的字符串。
        curl_setopt( $curl, CURLOPT_HEADER, false);
        //curl_setopt ($ch, CURLOPT_REFERER, "http://www.163.com/ ");
        curl_setopt($curl, CURLOPT_URL, self::getUrl());
        //将curl_exec()获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        //用来告诉 PHP 在成功连接服务器前等待多久（连接成功之后就会开始缓冲输出），这个参数是为了应对目标服务器的过载，下线，或者崩溃等可能状况
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);


        //避免https 的ssl验证
        if(stripos($url,"https://")!==FALSE){
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);        //不检查证书
            curl_setopt($curl, CURLOPT_SSLVERSION, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        }
    //    $file = curl_exec($ch);
        self::$curlError = curl_error($curl);

        list($content, $status) = [curl_exec($curl), curl_getinfo($curl), curl_close($curl)];
        self::$status = $status;
        self::$headerStr = trim(substr($content, 0, $status['header_size']));
        $content = trim(substr($content, $status['header_size']));
        if((intval($status["http_code"]) === 200)){
            return self::base64SaveFile($url, $content);
        }else{
            return self::$curlError;
        }
    }

    public static function base64SaveFile($url, $file)
    {
        //项目根目录
        $path = __DIR__ . '/curlFileTemp/';
        if(!self::mkdirs($path)){
            self::$curlError = "创建目录失败！";
            return self::$curlError;
        }
        $ext = pathinfo($url, PATHINFO_BASENAME);
        $ext = explode('.',$ext);
        $filename = uniqid() .'.'. end($ext);
        $resource = fopen( $path .$filename, 'a');
        fwrite($resource, $file);
        fclose($resource);
        return true;
    }
    protected function mkdirs($path){
        if (is_dir($path)){
            return true;
        }else{
            //第三个参数是“true”表示能创建多级目录，iconv防止中文目录乱码
            $res=mkdir(iconv("UTF-8", "GBK", $path),0777,true);
            if ($res){
                return true;
            }else{
                //创建目录失败！
                return false;
            }
        }
    }

    public static function request($url, $method = 'get', $data = array(), $header = false, $timeout = 15)
    {
        self::$status = null;
        self::$curlError = null;
        self::$headerStr = null;

        $curl = curl_init($url);
        $method = strtoupper($method);
        //请求方式
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        //post请求
        if ($method == 'POST') curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        //超时时间
        curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
        //设置header头
        if ($header !== false) curl_setopt($curl, CURLOPT_HTTPHEADER, $header);

        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        //返回抓取数据
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //输出header头信息
        curl_setopt($curl, CURLOPT_HEADER, true);
        //TRUE 时追踪句柄的请求字符串，从 PHP 5.1.3 开始可用。这个很关键，就是允许你查看请求header
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        //https请求
        if (1 == strpos("$" . $url, "https://")) {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }
        self::$curlError = curl_error($curl);

        list($content, $status) = [curl_exec($curl), curl_getinfo($curl), curl_close($curl)];
        self::$status = $status;
        self::$headerStr = trim(substr($content, 0, $status['header_size']));
        $content = trim(substr($content, $status['header_size']));
        return (intval($status["http_code"]) === 200) ? $content : false;
    }

    /*
     * 模拟常用浏览器的useragent
     */
    public static function getAgent()
    {
        $agentarry = [
            "微信内置浏览器"                    =>      "Mozilla/5.0 (Linux; Android             6.0; 1503-M02 Build/MRA58K) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile MQQBrowser/6.2 TBS/036558 Safari/537.36 MicroMessenger/6.3.25.861 NetType/WIFI Language/zh_CN",
            "iPhone11"                        =>       "Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_3 like Mac OS X) AppleWebKit/603.3.8 (KHTML, like Gecko) Mobile/14G60 MicroMessenger/6.5.18 NetType/WIFI Language/en",
            "华为P9全网通"                     =>         "Mozilla/5.0 (Linux; Android 7.0; EVA-AL00 Build/HUAWEIEVA-AL00; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.49 Mobile MQQBrowser/6.2 TBS/043508 Safari/537.36 MicroMessenger/6.5.13.1100 NetType/WIFI Language/zh_CN",
            "小米5X"                          =>         "Mozilla/5.0 (Linux; U; Android 7.1.2; zh-cn; MI 5X Build/N2G47H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.146 Mobile Safari/537.36 XiaoMi/MiuiBrowser/9.2.2",
            "一加手机3"                       =>          "Mozilla/5.0 (Linux; Android 7.1.1; ONEPLUS A3000 Build/NMF26F; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.49 Mobile MQQBrowser/6.2 TBS/043508 Safari/537.36 MicroMessenger/6.5.13.1100 NetType/WIFI Language/zh_CN",
            "努比亚Z11"                       =>         "Mozilla/5.0 (Linux; U; Android 6.0.1; zh-cn; NX531J Build/MMB29M) AppleWebKit/537.36 (KHTML, like Gecko)Version/4.0 Chrome/37.0.0.0 MQQBrowser/6.8 Mobile Safari/537.36",
            "小米5s"                          =>         "Mozilla/5.0 (Linux; Android 6.0.1; MI 5s Build/MXB48T; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.49 Mobile MQQBrowser/6.2 TBS/043508 Safari/537.36 V1_AND_SQ_7.2.0_730_YYB_D QQ/7.2.0.3270 NetType/WIFI WebP/0.3.0 Pixel/1080",
            "华为nova"                        =>         "Mozilla/5.0 (Linux; Android 7.0; HUAWEI CAZ-AL10 Build/HUAWEICAZ-AL10; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.49 Mobile MQQBrowser/6.2 TBS/043508 Safari/537.36 V1_AND_SQ_7.1.0_692_YYB_D QQ/7.1.0.3175 NetType/WIFI WebP/0.3.0 Pixel/1080",
            "联想ZUK Z2 Pro"                  =>         "Mozilla/5.0 (Linux; Android 7.0; ZUK Z2121 Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.49 Mobile MQQBrowser/6.2 TBS/043508 Safari/537.36 V1_AND_SQ_7.2.0_730_YYB_D QQ/7.2.0.3270 NetType/4G WebP/0.3.0 Pixel/1080",
            "魅蓝note 3"                      =>        "Mozilla/5.0 (Linux; Android 5.1; m3 note Build/LMY47I; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/48.0.2564.116 Mobile Safari/537.36 T7/9.3 baiduboxapp/9.3.0.10 (Baidu; P1 5.1)",
            "三星GALAXY S8+"                  =>        "Mozilla/5.0 (Linux; U; Android 7.0; zh-CN; SM-G9550 Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/40.0.2214.89 UCBrowser/11.7.0.953 Mobile Safari/537.36",
            "魅族MX6	"                        =>        "Mozilla/5.0 (Linux; Android 6.0; MX6 Build/MRA58K; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.49 Mobile MQQBrowser/6.2 TBS/043508 Safari/537.36 MicroMessenger/6.5.13.1100 NetType/4G Language/zh_CN",
            "vivo Xplay5A"                    =>       "Mozilla/5.0 (Linux; Android 5.1.1; vivo Xplay5A Build/LMY47V; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/48.0.2564.116 Mobile Safari/537.36 T7/9.3 baiduboxapp/9.3.0.10 (Baidu; P1 5.1.1)",
            "三星GALAXY C7"                   =>        "Mozilla/5.0 (Linux; U; Android 6.0.1; zh-CN; SM-C7000 Build/MMB29M) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/40.0.2214.89 UCBrowser/11.6.2.948 Mobile Safari/537.36",
            "三星GALAXY S8"                   =>         "Mozilla/5.0 (Linux; U; Android 7.0; zh-CN; SM-G9500 Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/40.0.2214.89 UCBrowser/11.7.0.953 Mobile Safari/537.36",
            "荣耀8青春版"                     =>         "Mozilla/5.0 (Linux; U; Android 7.0; zh-CN; PRA-AL00 Build/HONORPRA-AL00) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/40.0.2214.89 UCBrowser/11.7.0.953 Mobile Safari/537.36",
            "UCOpenwave"                      =>      "Openwave/ UCWEB7.0.2.37/28/999",
            "UC Opera"                        =>      "Mozilla/4.0 (compatible; MSIE 6.0; ) Opera/UCWEB7.0.2.37/28/999",
            "小米4S"                          =>        "Mozilla/5.0 (Linux; U; Android 5.1.1; zh-cn; MI 4S Build/LMY47V) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.146 Mobile Safari/537.36 XiaoMi/MiuiBrowser/9.1.3",
            "OPPO R12"                       =>          "Mozilla/5.0 (Linux; U; Android 7.1.1; zh-CN; OPPO R11 Build/NMF26X) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/40.0.2214.89 UCBrowser/11.7.0.953 Mobile Safari/537.36",
            "iPhone2"                        =>          "Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_3 like Mac OS X) AppleWebKit/603.3.8 (KHTML, like Gecko) Mobile/14G60 MicroMessenger/6.5.7 NetType/WIFI Language/zh_CN",
        ];
        return $agentarry[array_rand($agentarry,1)];
    }

    public static function proxyStatus(array $args){
        $url = $args['url'];
        $curl = curl_init($url);
        // CURLOPT_TIMEOUT 可以设置为100秒，如果文件100秒内没有下载完成，脚本将会断开连接    ,如果想让人卡死  设置的大一点
        curl_setopt($curl, CURLOPT_TIMEOUT, 1);

        curl_setopt($curl, CURLOPT_PROXY,$args['proxy']);//设置通过的HTTP代理服务器

        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        //返回抓取数据
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //输出header头信息
        curl_setopt($curl, CURLOPT_HEADER, true);
        //TRUE 时追踪句柄的请求字符串，从 PHP 5.1.3 开始可用。这个很关键，就是允许你查看请求header
      //  curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        //https请求
        if (1 == strpos("$" . $url, "https://")) {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }
        self::$curlError = curl_error($curl);
        list($content, $status) = [curl_exec($curl), curl_getinfo($curl), curl_close($curl)];
        return (intval($status["http_code"]) === 200) ? 200 : -1;
    }
    /**
     * @return mixed
     */
    public static function getCurlError()
    {
        return self::$curlError;
    }

    /**
     * @return mixed
     */
    public static function getHeaderStr()
    {
        return self::$headerStr;
    }

    /**
     * @return mixed
     */
    public static function getStatus()
    {
        return self::$status;
    }

    /**
     * @return mixed
     */
    public static function getUrl()
    {
        return self::$url;
    }

    /**
     * @param mixed $url
     */
    public static function setUrl($url)
    {
        self::$url = $url;
    }

    /**
     * @return mixed
     */
    public static function getIp()
    {
        return self::$ip;
    }

    /**
     * @param mixed $ip
     */
    public static function setIp($ip)
    {
        self::$ip = $ip;
    }

    /**
     * @return mixed
     */
    public static function getPort()
    {
        return self::$port;
    }

    /**
     * @param mixed $port
     */
    public static function setPort($port)
    {
        self::$port = $port;
    }

    /**
     * @return string
     */
    public static function getProxy()
    {
        return self::$proxy;
    }

    /**
     * @param string $proxy
     */
    public static function setProxy($proxy)
    {
        self::$proxy = $proxy;
    }

}
